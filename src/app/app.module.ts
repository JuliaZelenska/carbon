import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ZawodnikComponent } from './zawodnik/zawodnik.component';
import { HomepageComponent } from './homepage/homepage.component';
import { SilesiasportComponent } from './silesiasport/silesiasport.component';
import { KontaktComponent } from './kontakt/kontakt.component';
import { GlownyComponent } from './glowny/glowny.component';
import { KafelekComponent } from './glowny/kafelek/kafelek.component';

@NgModule({
  declarations: [
    AppComponent,
    ZawodnikComponent,
    HomepageComponent,
    SilesiasportComponent,
    KontaktComponent,
    GlownyComponent,
    KafelekComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
