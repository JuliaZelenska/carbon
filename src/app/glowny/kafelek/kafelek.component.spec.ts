import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KafelekComponent } from './kafelek.component';

describe('KafelekComponent', () => {
  let component: KafelekComponent;
  let fixture: ComponentFixture<KafelekComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KafelekComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KafelekComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
