import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-kafelek',
  templateUrl: './kafelek.component.html',
  styleUrls: ['./kafelek.component.scss']
})
export class KafelekComponent implements OnInit {
  @Input() nazwa: string;
  @Input() tlo: string;
  constructor() {}

  ngOnInit() {}
}
