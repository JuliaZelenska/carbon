import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SilesiasportComponent } from './silesiasport.component';

describe('SilesiasportComponent', () => {
  let component: SilesiasportComponent;
  let fixture: ComponentFixture<SilesiasportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SilesiasportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SilesiasportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
