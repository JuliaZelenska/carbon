import { Component, OnInit } from '@angular/core';
import { Zawodnik } from 'src/models/zawodnik.model';

@Component({
  selector: 'app-zawodnik',
  templateUrl: './zawodnik.component.html',
  styleUrls: ['./zawodnik.component.scss']
})
export class ZawodnikComponent implements OnInit {
  zawodnicy: Zawodnik[];

  constructor() {
    this.zawodnicy = [
      {
        imie: 'Tomasz Skwarek',
        numer: 1,
        ksywa: 'Sqwaru',
        zdjecie: '../../assets/img/zawodnicy/1.jpg',
        opis: 'lorem ipsum'
      },
      {
        imie: 'Sebastian Glonek',
        numer: 2,
        ksywa: 'Glonek',
        zdjecie: '../../assets/img/zawodnicy/2.jpg',
        opis: 'lorem ipsum'
      }
    ];
  }

  ngOnInit() {}
}
