import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ZawodnikComponent } from './zawodnik.component';

describe('ZawodnikComponent', () => {
  let component: ZawodnikComponent;
  let fixture: ComponentFixture<ZawodnikComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ZawodnikComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ZawodnikComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
