import { GlownyComponent } from './glowny/glowny.component';
import { KontaktComponent } from './kontakt/kontakt.component';
import { ZawodnikComponent } from './zawodnik/zawodnik.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomepageComponent } from './homepage/homepage.component';
import { SilesiasportComponent } from './silesiasport/silesiasport.component';

const routes: Routes = [
  {
    path: '',
    component: HomepageComponent
  },
  {
    path: 'zawodnicy',
    component: ZawodnikComponent
  },
  {
    path: 'silesiasport',
    component: SilesiasportComponent
  },
  {
    path: 'kontakt',
    component: KontaktComponent
  },
  {
    path: 'glowny',
    component: GlownyComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
