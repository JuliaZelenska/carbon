export interface Zawodnik {
  imie: string;
  zdjecie: string;
  numer: number;
  opis: string;
  ksywa: string;
}
